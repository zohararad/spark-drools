package com.zohararad.spark

import java.util.Calendar

import com.zohararad.models.{Event, Ticket}
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkContext, SparkConf}
import org.kie.api.{KieBase, KieServices}
import org.slf4j.LoggerFactory
import scala.util.Random

object DroolsJob {

  val logger = LoggerFactory.getLogger(DroolsJob.getClass())
  val sparkConf = new SparkConf().setAppName(getClass.toString)

  def main(args: Array[String]): Unit = {
    val sc = getSparkContext
    val kb = getKB
    val kbBroadcast = sc.broadcast(kb)
    val (tickets, events) = getInitialState(sc)
    val ticketsRDD = getTicketsRDD(tickets, events)
    val processedTickets = processTickets(kbBroadcast, ticketsRDD)
    processedTickets.foreach{ t =>
      logger.info("Ticket %s | Status: %s".format(t.id, t.status))
    }
  }

  /**
    * Processes tickets and their events. Iterates over RDD and sends each tuple for processing
    * @param kbBroadcase Spark broadcast variable containing Drools Kie knowledge-base
    * @param tickets RDD of tuples where each tuple contains a ticket and its respective events
    * @return RDD[Ticket]
    */
  def processTickets(kbBroadcase: Broadcast[KieBase], tickets: RDD[(String, Ticket, List[Event])]) = {
    tickets.map{ t =>
      val kb = kbBroadcase.value
      processTicket(kb, t._2, t._3)
    }
  }

  /**
    * Processes each ticket in Drools. Inserts ticket and its events to Drools session and then fires all rules.
    * @param kb Drools Kie knowledge-base
    * @param ticket ticket to process
    * @param events ticket events that may modify ticket state
    * @return Ticket
    */
  def processTicket(kb: KieBase, ticket: Ticket, events: List[Event]): Ticket = {
    val session = kb.newKieSession()
    session.setGlobal("logger", logger)
    logger.info("Ticket %s | Status: %s".format(ticket.id, ticket.status))
    session.insert(ticket)
    events.foreach{ e =>
      session.insert(e)
    }
    session.fireAllRules()
    session.dispose()
    ticket
  }

  /**
    * Get an RDD of tickets, with their respective events, sorted by ascending timestamp. Initializes new tickets in case
    * no ticket is available for a given event
    * @param tickets tickets RDD
    * @param events events RDD
    * @return RDD[ Tuple3[ String, List[Ticket], List[Event] ] ]
    */
  def getTicketsRDD(tickets: RDD[Ticket], events: RDD[Event]) = {
    val eventsRDD = events.groupBy(e => e.ticketId).map{ t =>
      ( t._1, t._2.toList.sortWith( (a,b) => a.ts < b.ts) )
    }
    val ticketsRDD = tickets.groupBy(t => t.id)

    eventsRDD.leftOuterJoin(ticketsRDD).map{ t =>
      val ticketId = t._1
      val events = t._2._1
      val ticket = t._2._2 match {
        case Some(l: List[Ticket]) => l.head
        case _ => Ticket.fromEvent(events.head)
      }
      (ticketId, ticket, events)
    }
  }

  /**
    * Creates an initial collection of tickets and events for the code to run with
    * @param sc Spark Context which is used to wrap data in RDDs
    * @return Tuple2[ RDD[Ticket], RDD[Event] ]
    */
  def getInitialState(sc: SparkContext) = {
    val cal = Calendar.getInstance()
    cal.set(Calendar.MINUTE, -140)
    val ids = 1 to 50 map { i: Int => Random.alphanumeric.take(32).mkString }
    val tickets = 0 to 49 map { i: Int =>
      val status: String = getStatus(i)
      val id: String = ids(i)
      Ticket(id, status)
    }
    val events = 0 to 149 map { i: Int =>
      val idIdx = i % 50
      val ticketId = ids(idIdx)
      val status: String = getStatus(i)
      val s = cal.get(Calendar.SECOND)
      cal.set(Calendar.SECOND, s + Random.nextInt(5))
      val ts = cal.getTimeInMillis
      Event(ticketId, status, ts)
    }
    (sc.parallelize(tickets), sc.parallelize(events))
  }

  /**
    * Ticket status getter. Returns ticket status based on remainder of passed integer
    * @param i integer used to select relevant ticket status
    * @return String
    */
  def getStatus(i: Int): String = {
    i % 3 match {
      case 0 => Ticket.OPEN
      case 1 => Ticket.PENDING
      case 2 => Ticket.CLOSED
    }
  }

  /**
    * Spark Context getter
    * @return SparkContext
    */
  def getSparkContext: SparkContext = {
    sparkConf.setMaster("local[6]")
    sparkConf.set("spark.driver.memory", "512m")
             .set("spark.core.max", "6")
             .set("spark.default.parallelism", "18")
    new SparkContext(sparkConf)
  }

  /**
    * Drools Kie knowledge-base getter
    * @return KieBase
    */
  def getKB: KieBase = {
    val kieServices = KieServices.Factory.get()
    val kieContainer = kieServices.getKieClasspathContainer
    kieContainer.getKieBase()
  }
}