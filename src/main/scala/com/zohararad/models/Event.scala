package com.zohararad.models

case class Event(ticketId: String, ticketStatus: String, ts: Long)
