package com.zohararad.models

import scala.beans.{BeanProperty, BeanInfo}

@BeanInfo
case class Ticket(id: String,
                  @BeanProperty var status: String )

object Ticket{
  val OPEN = "open"
  val PENDING = "pending"
  val CLOSED = "closed"

  def fromEvent(e: Event): Ticket = {
    Ticket(e.ticketId, e.ticketStatus)
  }
}