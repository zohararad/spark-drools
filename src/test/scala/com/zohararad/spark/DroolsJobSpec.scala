package com.zohararad.spark

import com.zohararad.UnitSpec
import com.zohararad.models.Ticket

class DroolsJobSpec extends UnitSpec{

  lazy val sc = DroolsJob.getSparkContext
  lazy val kb = DroolsJob.getKB

  "Tickets RDD" should "contain ticket events sorted by ascending timestamp" in {
    val (tickets, events) = DroolsJob.getInitialState(sc)
    val ticketsRDD = DroolsJob.getTicketsRDD(tickets, events)
    val t = ticketsRDD.first()
    val eventA = t._3.head
    val eventB = t._3.last
    eventA.ts should be < eventB.ts
  }

  "Drools" should "change ticket state" in {
    val (tickets, events) = DroolsJob.getInitialState(sc)
    val ticketsRDD = DroolsJob.getTicketsRDD(tickets, events)
    val t = ticketsRDD.first()
    val ticket = t._2
    val ticketEvents = t._3
    // ensure ticket starts with a different status than first event
    ticket.status = ticketEvents.head.ticketStatus match {
      case Ticket.OPEN => Ticket.PENDING
      case Ticket.PENDING => Ticket.CLOSED
      case Ticket.CLOSED => Ticket.OPEN
    }

    ticket.status shouldNot be (ticketEvents.head.ticketStatus)
    // process ticket with Drools and check status change
    val processedTicket = DroolsJob.processTicket(kb, ticket, ticketEvents)
    processedTicket.status should be (ticketEvents.last.ticketStatus)
  }

}
