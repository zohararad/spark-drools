name := "spark-drools"

version := "1.0"

scalaVersion := "2.10.6"

organization := "com.zohararad"

libraryDependencies ++= Seq(
  "drools-compiler",
  "drools-core",
  "drools-decisiontables",
  "knowledge-api"
).map("org.drools" % _ % "6.3.0.Final")

libraryDependencies ++= Seq(
  "spark-core",
  "spark-sql",
  "spark-streaming"
).map("org.apache.spark" %% _ % "1.6.0" % "provided" )

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.2.6" % "test"
)